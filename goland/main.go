package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	// Crear un enrutador Gin
	router := gin.Default()

	// Definir la ruta GET /
	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})
	})

	// Definir la ruta POST /sumar
	router.POST("/sumar", func(c *gin.Context) {
		// Obtener los parámetros num1 y num2 del cuerpo de la solicitud
		var request struct {
			Num1 int `json:"num1" binding:"required"`
			Num2 int `json:"num2" binding:"required"`
		}

		if err := c.ShouldBindJSON(&request); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": "Los parámetros num1 y num2 son requeridos y deben ser números enteros",
			})
			return
		}

		// Sumar los dos números
		result := request.Num1 + request.Num2

		// Retornar el resultado como JSON
		c.JSON(http.StatusOK, gin.H{
			"resultados": result,
		})
	})

		// Definir la ruta POST /sumar
	router.POST("/restar", func(c *gin.Context) {
		// Obtener los parámetros num1 y num2 del cuerpo de la solicitud
		var request struct {
			Num1 int `json:"num1" binding:"required"`
			Num2 int `json:"num2" binding:"required"`
		}

		if err := c.ShouldBindJSON(&request); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": "Los parámetros num1 y num2 son requeridos y deben ser números enteros",
			})
			return
		}

		// Sumar los dos números
		result := request.Num1 - request.Num2

		// Retornar el resultado como JSON
		c.JSON(http.StatusOK, gin.H{
			"resultados": result,
		})
	})


	// Iniciar el servidor en el puerto 5000
	if err := router.Run(":6000"); err != nil {
		fmt.Println("Error al iniciar el servidor:", err)
	}
}
