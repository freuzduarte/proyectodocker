from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['GET'])
def root():
    return 'OK', 200

@app.route('/sumar', methods=['POST'])
def sumar():
    try:
        num1 = int(request.json['num1'])
        num2 = int(request.json['num2'])
        resultado = num1 + num2
        return jsonify({'resultado': resultado})
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@app.route('/restar', methods=['POST'])
def restar():
    try:
        num1 = int(request.json['num1'])
        num2 = int(request.json['num2'])
        resultado = num1 - num2
        return jsonify({'resultado': resultado})
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6000)